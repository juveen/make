/**
Enqueue scripts
*/

function scripts_para_enfileirar() {
    //wp_enqueue_style( 'core', 'style.css', false );
    wp_register_script( 'nome-do-script', get_template_directory_uri() . '/assets/js/nome.js', array( 'jquery' ) );
    wp_enqueue_script( 'nome-do-script' ); 
}

function themeslug_enqueue_script() {
	wp_enqueue_script( 'my-js', 'filename.js', false );
}

add_action( 'wp_enqueue_scripts', 'scripts_para_enfileirar' );

